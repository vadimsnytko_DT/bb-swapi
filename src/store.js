import Vue from 'vue'
import Vuex from 'vuex'
import http from '@/helpers/axiosInstance'
import cookieHelper from '@/helpers/cookieHelper'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    resources: {},
    lastData: {},
    loadingList: true,
    resourcesTranslation: {},
    cookie: { language: 'en', memo: {}},
  },
  mutations: {
    setResources(state, res){
      state.resources = res
    },
    setResourcesTranslation(state, res){
      state.resourcesTranslation = res
    },
    setLanguage: (state, lang) => {
      state.cookie.language = lang
      cookieHelper.setCookie( JSON.stringify(state.cookie))
    },
    setLastData: (state, data) => {
      state.lastData = data
    },
    setLoadingState: (state, loading) => {
      state.loadingList = loading
    },
    setCookieObject: (state, cookie) => {
      cookieHelper.setCookie( JSON.stringify(cookie))
      state.cookie = cookie
    },
    setMemoById: (state, data) => {
      if( !state.cookie.memo) state.cookie.memo = {}
      state.cookie.memo[data.id] = data.memo
      cookieHelper.setCookie( JSON.stringify(state.cookie))
    }
  },
  getters: {
    /**
     * Get translated resource. key => translated_value.
     * @param state
     * @returns {{}}
     */
    getResources: state => {
      let translatedResources = {}
      if (state.cookie.language === 'en') {
        Object.keys(state.resources).map((key) => {
          translatedResources[key] = key
        })
      }
      else {
        Object.keys(state.resources).map((key, index) => {
          translatedResources[key] = Object.values(state.resourcesTranslation)[index]
        })
      }
      return translatedResources
    },
    getLanguage: state => (state.cookie.language || 'en'),
    getResourceByKey: state => id => state.resources[id]
  },
  actions: {
    /**
     * Update store and cookie with language.
     * @param state
     * @param language
     */
    setUpdateLanguage(state, language) {
      state.commit('setLanguage', language)
      state.commit('setCookieObject', state.state.cookie)
    },
    /**
     * Initial resource update.
     * @param commit
     */
    updateResources({commit}){
      http.get('/api/')
        .then((data) => commit('setResources', data.data))
      http.get('/api/?format=wookiee')
        .then((data) => {
          commit('setResourcesTranslation', Object.keys(data.data))
        })
    },
    updateCookieObject({commit}){
      try {
        let cookie = JSON.parse(cookieHelper.getCookie())
        commit('setCookieObject', cookie)
// eslint-disable-next-line
      } catch (e) {}
    },
    updateCurrentList({commit}, query){
      commit('setLoadingState', true)
      http.get(query)
        .then(data => {
          let list = data.data
          if (typeof list === 'string') {
            list = JSON.parse(list)
          }
          commit('setLastData', list)
        })
        .catch((e) => {
          console.log('Wrong api response: ' + e)
          commit('setLastData', [])
        })
        .finally(()=> {
          commit('setLoadingState', false)
        })
    }
  },
})

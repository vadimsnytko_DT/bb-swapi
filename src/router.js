import Vue from 'vue'
import Router from 'vue-router'
import CardList from './components/CardList'
import Home from './components/Home.vue'
Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'SWAPI',
      component: Home
    },
    {
      path: '/resource/:id',
      name: 'Cards',
      component: CardList,
    },
  ]
})

import Axios from 'axios'
export default Axios.create({
  baseURL: 'https://swapi.co/',
  timeout: 5000,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
  },
  transformResponse: [function (data) {
    return JSON.parse(data.replace(new RegExp('whhuanan', 'gmi'), 'null').replace(new RegExp("\\\\", 'gmi'), ''));
  }],
});
# BB-SWAPI

Uses [swapi.co](https://swapi.co/) as API. 
* Show all resources
* Show resource entities
* Ability to add memo and edit it. Saved after reload a page
* Switch between English and Wookie

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```